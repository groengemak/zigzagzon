# KNMI data source

[Daily data](https://daggegevens.knmi.nl/klimatologie/daggegevens)

Period 10y covering the 10-day blocks:
20121201 — 20230131

Measurements:
TG, etmaalgemiddelde temperatuur [in 0.1 ⁰C]
TX, maximum temperatuur [in 0.1 ⁰C]
SQ, zonneschijnduur [in 0.1h]
SP, percentage van de langst mogelijke zonneschijnduur [%]
Q, globale straling [J/cm2]
NG, etmaalgemiddelde bewolking (9 = bovenlucht onzichtbaar)
UG, etmaalgemiddelde relatieve vochtigheid [%]
UN, minimale relatieve vochtigheid [%]

Weerstations:
290, Twenthe

Formaat:
CSV

Download naar 2 bestanden, met de kop en tabeldata en ":1,$s/  *//g" op de data:
knmi-kop.txt
knmi-10y.csv

Analysedoel:
 1. Spiegel de metingen nog niet, dat dataverlies kunnen we later nog incasseren
 2. Verwerk metingen per datumblok van ±4d, dus 9 metingen per datum (we verliezen 5.25 d/y)
 3. Test of er saillante verschillen zijn tussen spiegeldata (nasleep door vocht?)
 4. Combineer 9 dagen over 10 jaar, dus 90 meetpunten; of met spiegeling 180 meetpunten
 5. Bepaal met inverse CDF: de lage waarde waaronder 2.5% valt, de hoge waarboven 97.5% valt
 6. Gebruik de hoge waarde als rechtstreekse inval, het lage als bewolkt
 7. Meng de uiterste metingen tot het gemiddelde, combineer berekeningen ook zo

Beter berekenbaar:
- De hoeveelheid diffuus licht, voor afzonderlijke doorberekening van de opbrengst
- De gangbare samenstelling, per datumblok, tussen helder en diffuus licht
- Daaruit een combinatie van de twee opbrengsten

Python3:
- Leest kop in als keywords voor een dict; elke regel krijgt zo'n dict bij iteratie

    >>> import csv
    >>> dr = csv.DictReader (open ('knmi-10y.csv'))
    >>> for d in dr:
    ...     print (d)

- Kan datums met offsets aanpassen, en schrikkelt nergens van

    >>> import datetime
    >>> d = datetime.date (2022,3,2)
    >>> n = d - datetime.timedelta (4)
    >>> x = d + datetime.timedelta (4)
    >>> print ('date=%r, min=%r, max=%r' % (d,n,x))
    >>> print ('3*True, 3*False: ',n<d,d<x,n<x,x<n,x<d,d<n)

- Kan [confidence intervals bepalen](https://www.statology.org/confidence-intervals-python/) met NumPy / SciPy **maarrrr** dat is niet de juiste waarde om te bepalen

    >>> import numpy as np
    >>> import scipy.stats as st
    >>> 
    >>> #define sample data
    >>> data = [12, 12, 13, 13, 15, 16, 17, 22, 23, 25, 26, 27, 28, 28, 29]
    >>> 
    >>> #create 95% confidence interval for population mean weight
    >>> st.t.interval(alpha=0.95, df=len(data)-1, loc=np.mean(data), scale=st.sem(data)) 
    >>> 
    (16.758, 24.042)

- Heeft een class [statistics.NormalDist](https://docs.python.org/3/library/statistics.html#statistics.NormalDist) waarmee het "met de hand" kan, en met de inv_cdf functie

    >>> from statistics import NormalDist
    >>> 
    >>> #define sample data
    >>> data = [12, 12, 13, 13, 15, 16, 17, 22, 23, 25, 26, 27, 28, 28, 29]
    >>> nd = NormalDist.from_samples (data)
    >>> nd.inv_cdf (0.025)
    7.509285008712771
    >>> nd.inv_cdf (0.975)
    33.290714991287224

