FIGS += zigzagzon-fig1-design.pdf
FIGS += zigzagzon-fig2-direct.pdf
FIGS += zigzagzon-fig3-reflected.pdf
FIGS += zigzagzon-fig4-direct-shaded.pdf
FIGS += zigzagzon-fig5-design.pdf
FIGS += zigzagzon-fig6-direct.pdf
FIGS += zigzagzon-fig7-reflected.pdf
FIGS += zigzagzon-fig8-fresnel.pdf
FIGS += zigzagzon-fig9-fresnel-ratios.pdf
FIGS += zigzagzon-fig10-blinds-design.pdf

all: zigzagzon.pdf

%.eps: %.dia
	dia --export=$@ $<

%.pdf: %.eps
	epstopdf $< $@

zigzagzon.pdf: zigzagzon.tex intro.tex why.tex gonio.tex $(FIGS)
	pdflatex $<

booklet.ps: zigzagzon.pdf
	pdf2ps $< /dev/stdout | psbook | psnup -n 2 | psmandup -o $@
