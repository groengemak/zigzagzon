# Optimised flat-roof Installation in the Netherlands

> *This is an installation that sacrifices a lot of heat during the hot season.
> As a result, a quite different boiler/heatpipe ratio is possible.  When the
> boiler is smaller, the temperatures reached in it can be higher, making them
> more useful.  In addition, this installation greatly improves the heat that
> is captured during the cold season.*


## Standard Setup

Standard setups place the heat pipes under a 45⁰ slope, facing South.
There is no reflector in the standard setup.  In terms of the goniometric
design parameters, that is α=45⁰, β=0⁰ and A2=0.  The relation between
light and captured heat is rather direct, as shown below.

![Graphical Form for Standard Setup](zigzagzon-standard-45-0.svg)

## Optimal Setup

This setup uses goniometric design parameters α=105⁰, β=-20⁰ and A2=6.5m.
You cannot make heat pipes hang over by 105⁰, of course, but you can rotate
the mounting gear and mount the heat pipes under 75⁰.  The heat pipes are not
looking North however; they are setup to look South, but downward.  Also,
it is not possible to delve a reflector into the roof at -20⁰ but instead a
Fresnel reflector is used, at the expense of extra length A2 horizontally
in front of the heat pipes.

This seemingly silly design looses most Sun from a high position, but will
capture most from a low position.  The reflector adds a lot as well, but
for high Sun positions the reflection would pass by the heat pipes, while
low Sun positions are reflected much better.  If the full 6.5m are not
available in front of the heat pipes then less will cut short the benefits
in only the coldest time of year; the rest will be as it was before.

We evaluated this design for the Dutch situation, where Summer Sun climbs
as high as 63⁰ while Winter Sun does not make it above 14⁰.  The results
in comparison to the standard design are striking:

| Dates up | Dates down | Light [MJ/d] | Standard [MJ/d] | Optimal [MJ/d] |
|---------:|-----------:|-------------:|----------------:|---------------:|
| Dec, 21  | Dec, 21    |  43          |  36             |  70            |
| Dec, 30  | Dec, 12    |  43          |  37             |  72            |
| Jan, 08  | Dec, 03    |  46          |  40             |  75            |
| Jan, 17  | Nov, 24    |  50          |  44             |  81            |
| Jan, 26  | Nov, 15    |  56          |  51             |  88            |
| Feb, 04  | Nov, 06    |  64          |  58             |  95            |
| Feb, 13  | Oct, 28    |  72          |  67             | 103            |
| Feb, 22  | Oct, 19    |  81          |  77             | 109            |
| Mar, 03  | Oct, 10    |  90          |  88             | 114            |
| Mar, 12  | Oct, 01    | 100          |  98             | 115            |
| Mar, 21  | Sep, 22    | 110          | 109             | 114            |
| Mar, 30  | Sep, 13    | 118          | 118             | 110            |
| Apr, 08  | Sep, 04    | 127          | 127             | 102            |
| Apr, 17  | Aug, 26    | 134          | 134             |  93            |
| Apr, 26  | Aug, 17    | 140          | 139             |  82            |
| May, 05  | Aug, 08    | 146          | 143             |  71            |
| May, 14  | Jul, 29    | 150          | 146             |  60            |
| May, 23  | Jul, 20    | 153          | 147             |  51            |
| Jun, 03  | Jul, 09    | 155          | 148             |  44            |
| Jun, 12  | Jun, 30    | 156          | 149             |  40            |
| Jun, 21  | Jun, 21    | 157          | 149             |  38            |

These figures are based on 8 m2 heat pipes with an annual total yield of
about 40 GJ.  The dates follow linear time, with 9 or 10 days between rows.
The range goes up from the Winter Solstice to Summer Solstice, and returns to
the Winter Solstice in opposite direction; in other words, this table represents
half a year and mirrors the other side.

The light shown is the solar radiation that may be expected at the given dates.
This is an estimate.  The two situations compared are the standard and optimal
situations described above.

Note that the standard solution follows the offering of light, resulting in a
lot of heat being captured in Summer, when it is hardly useful.  This leads to
the requirement for large boilers, but in Winter time we distribute the same
heat throughout the same large boiler, resulting in lower temperatures (and
much more energy spent to keep a large body of water safe from Legionella).
This is hardly usable when one's aim is to live in an energy-neutral home.

The total annual yield is about 15% lower than in the standard setup, but since
the heat is available when it can be used, and suppressed when it is mostly
a nuisance, the day-to-day yield is much more useful.  This is especially
important because heat is difficult to store or transport.  In addition, a 25%
smaller boiler can be used, resulting in 33% more temperature rise for the
same solar heat.

![Graphical Form for the Optimised Setup](zigzagzon-optimal-105-min20.svg)

The optimal solution greatly reduces the output during Summer, while there is
a good gain during Winter, in spite of the lower light conditions during that
season.  Spring and Autumn get caught up in the game as well, yielding the
highest energy output.  To be honest, the heat is hardly problematic in those
seasons; for one, these are times of fast solar changes and so they lead to
more wind and rain; secondly, it is easy to ventilate to let the heat out
than it is in the heat of Summer.  Finally, having ample heat in Spring may
help to grow young seedlines, and ample heat in Autumn may be useful to
dry one's produce.

Taking a modest 15-minute shower takes about 11 MJ.  In the standard situation
however, the temperature is usually not high enough, while this stands a
serious chance in the optimal situation.  The leftover after a daily (...)
shower in the optimal situation would be 59 MJ/d, enough to produce the
heat that a 700-Watt infrared panel produces in 23.4 hours.  Or, in terms
of floor heating at 25 W/m2, it can run 656 m2.h, so 25 m2 for 26 hours
or 50 m2 for 13 hours.  Note that 25 W/m2 is advised to make the floor
warm enough, but it is not meant to be run fulltime.

*There is no certainty.* That's the thing with solar energy, it can only
be used opportunistically.  These numbers however, indicate that there is
a good chance that a simple and cheap backup system such as infrared panels,
given that it is appropriately dimensioned, can provide the desired certainty
of a warm home without actually needing it very often.  A great part of an
energy-neutral solution for a home!

*Note how valuable this is.*  It works removes excess heat from Summer and
makes it available in Winter (and Spring and Autumn).  This `moving of energy'
between the seasons is one of the most useful ways to stop destroying our
climate.  In addition, this makes it much easier to decouple from fossil fuels,
which also benefits political stability and an autarkian lifestyle.  Finally,
energy saving is sound economic investment, usually with annual yields ~11%.

## Fresnel Reflectors

The paper is not quite correct at present, where it discusses Fresnel reflectors.
There is a need to make the slopes go less steeply with growing distance to
the heat pipes, or otherwise there will be stripes.  This actually is a degree
of freedom, in terms of which solar angles are passed without strips (but with
possible overlap, which is harmless).

One might choose a highly optimised annual profile; there is a minimal output
due to direct light and a maximum output due to the reflector size, but the
control within those constraints can be very interesting.

## Retrofitting in Standard Setups

Below is another parameterisation that is baffling.  Even though its total
annual output is only 5% below the standard setup, it is nonetheless not as
useful as the one above, because it collects heat in Summer and this may be
difficult to get rid of.

![Graphical Form for an Interesting Setup](zigzagzon-interesting-75-min15-2m.svg)

This setup may be easier to retrofit in standard setups, where it may help
to maximise Winter output.  Note that 75⁰ is a commonly heard angle
that is a maximum for the heat pipes or for their mounting gear.
It may also be helpful to correct installations that produce too much heat
during Summer.  Finally, it is a good change if ever the boiler needs to be
replaced (a 30% smaller one would then be advisable for the same heat in
Summer and much better day-to-day performance in the rest of the year).

If vertical mounting with a Fresnel reflector in front is an option, then
a figure similar to before can be had.

![Graphical Form for a Vertical Setup](zigzagzon-vertical-90-min15-6m.svg)

## Words of Caution

These are mid-day measurements, assuming no clouds.  In practice, the light
is often scattered by light, which may have an impact on the performance
for various slopes α and various reflector sizes A2.
