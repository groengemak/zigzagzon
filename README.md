# ZigZagZon — Zig Zag Solar

> *These work analyses the influence of reflectors placed above or before
> heat pipes, including Fresnel variants.  The work consists of a paper
> with goniometric derivations and a spreadsheet putting that to use.*

Heat pipes are highly efficient ways of harvesting solar heat.  They may
produce more interesting results when properly analysed.

A general design observation is that the heat from the hot season must
be stored, to avoid the transportation liquid from boiling and imposing
great pressure on the system.  This leads to boiler sizing to accommodate
the heat captured in the hot season.

The greatest utility for heat pipes is during the cold season.  During
this time, less heat is spread over the relatively large boiler.  The
result is that the temperatures reached are not very useful in the colder
days of the year.

There are a few ways out of this predicament:

  * Use boilers that vary in size between the seasons; currently there
    are no boilers that include pistons or balloons to remove part of
    their contents, but that would help.

  * Use a second-stage store, such as an extra storage vat or a concrete
    floor; this still means that superfluous heat gets stored in the hot
    season, but now temperatures can peak higher in the cold season.

  * Setup the heat pipes in a different manner from the customary 45⁰
    slope that faces South.  This is the topic of the work done here,

      - Place a reflector over or in front of heat pipes.  Use Fresnell
        reflectors to accommodate reflection angles in spite of
        physical angles (such as roof top surfaces).

      - Consider various slopes, including the top hanging towards South.

      - Aim to reduce heat captured in the hot season, thereby allowing
        more heat pipes and better performance in the cold season.

See [OPTIMISED.md](OPTIMISED.md) for a striking example of the benefits.
