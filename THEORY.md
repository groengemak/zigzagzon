# Theoretical Foundations of Reflectors and Heat Pipes

> *Yield graphs for heat pipe systems clearly show a waveform that has
> a period of half a year, rather than a full year.  This is normal,
> and we can use it to drive a theoretic model.*


## Product of Sine Waves

Heat pipes produce a certain amount of energy *D* due to direct radiation,
varying as a sine wave througout the year.

Reflectors reflect energy *R* to the heat pipes, again following a
sine wave pattern.  It is lucrative to choose angles such that this
compensates the direct radiation.

The total heat collected is a composition of *D* and *R*, where the
influence of *R* will vary throughout the year, because a variable
amount of heat will overshoot the heat pipes.  This means that the
influence of *R* is multiplied by another factor *X* that is a sine wave.

The output therefore looks like *D + R.X*.  And since *D*, *R* and *X*
all follow an annual cycle, the product *R.X* follows a semi-annual
cycle.  The total graph therefore is the sum of an annual cycle and a
semi-annual cycle.

The sine waves are not necessarily centered around zero.  That is due
to the limited range of angles covered by the Sun at mid-day.  We shall
use notation like *cld(D)* and *hot(R)* for the values during the Cold
and Hot Solstices, and *eqx(X)* for the values during both the
Equinoxes.


## Saddle Points

Saddle points are those points where a value is temporarily stable.
It may be an extreme, or later developments may continue a preceding
rise or drop.

The annual graphs show a number of saddle points, at well-defined moments:

  * Winter Solstice
  * Summer Solstice
  * Spring and Autumn Equinoxes

The solstices are radically different.  The equinoxes reap the same results,
so they are treated as one value of interest.  Our computational model only
computes the path from Winter Solstice, through Spring Equinox, to Summer
Solstice.

Our computation model treats the second half of the year as the mirror image
of this first half.  The only point of theoretical concern is that annual
totals and averages should count the solstices once, and the intermediate
values twice.


## Quick and Dirty

For fast calculations, it is sufficient to compute the output from direct
light and reflection in the three saddle points, and consider the path in
between as a cosine function.  When averages and totals are to be estimated,
the intermediate path may be considered a straight line.  Effectively, the
annual aggregates would be the sum of the three moments, with double weight
for equinox value.

The reason that this is not only quick, but also a bit dirty, is that the
precise computations may involve minimum or maximum boundaries, such as
caused by shaded areas, or partially caught light from the reflector.  It
is beneficial to consider such turning points, which are caused by the
slope of direct Sun light, so it is located on fixed and mirrorred moments
of the year.

Note that such `trend breaking' moments do not form saddle points; they
merely change the speed at which a sine wave develops.  This tends to be
a sharp change, as a result of the physical effect (introducing stripes
in a reflection, or moving light outside the heat pipe area).  The
different speeds may be viewed as a change of direction, from moving
towards one saddle point to a path moving towards another.  Once the
point of change is left behind, it will appear as though the source
was another saddle point.

There is no limit on the number of because `trend breaks'; each section
of a Fresnel reflector may trigger at another slope of direct Sun light.


## Diffuse Light Conditions

A separate computation should be made for diffuse light conditions, such
as those caused by clouds.  These represent comparable light from all
directions, but most of it is bound to come from above, except when light
is reflected.  Diffuse light conditions therefore also need separate
computations for direct and reflected light.

Meteorological information may give an idea of the likelyhood of diffuse
light versus direct light, can provide a statistical model, or a mere
average, for each date in the year.


## Mallability of Performance

TODO: STATIC OFFSET

Normally, the settings for an installation dictate the output.  Trial and
error can be used to search for a personal optimum.  Or, we might aim to
tune the output by reversing the computations.

The shape of the output is pretty much determined by the three saddle points,
with possible adaptations caused by trend breaks.  This means that we can
tune the output as well as we can tune the saddle points.

Can we reach any depth in the hot Solstice?  We could have the heat pipes setup
in line with the hot-season's mid-day Sun to come down to zero.  Reflection
might be setup such that the reflected light passes by the heat pipes at that time.

Can we reach any height in the cold Solstice?  Reflector sizes can be 0, so
no reflection is possible.  Higher values can be achieved with arbitrarily
large reflectors.  This works for any angle β that reflects some light during
the cold Solstice (and it is senseless to not reflect anything at that time).

Can we reach any height during the Equinoxes?  We have a good degree of choice
there too; the angle β can be taken arbitrarily for the cold Solstice, and may
well be chosen to have no impact during the hot Solstice; it does have an
impact on the Equinoxes, so it can be tuned to reach a desired performance
during the Equinoxes.

As a result, the performance is highly mallable, but there are practical
constraints due to size and the degree to which we are willing to
concentrate light.  Such results are very attractive, because it allows the
reflection of undesired heat in the form of light.  Light can travel back
into Space, whereas heat will be mostly caught under a blanket of CO2.


## Reverse Computations

The combined performance was described as *D + R.X* before.  Each of these
functions moves in an annual period between extremes in a sine wave, with
a middle point at the Equinoxes.  The *R.X* product is also a sine wave,
but with a semi-annual period, with middle points at the Solstices.

The saddle points combine low and high values of the various sine waves
as follows:

  * Cold Solstice: *cld(D) + cld(R).cld(X)*
  * Hot Solstice:  *hot(D) + hot(R).hot(X)*
  * Equinoxes:     *eqx(D) + eqx(R).eqx(X)*

Due to the semi-annual period, we know *hot(R).hot(X) = cld(R).cld(X)*
and also TODO:REALLY? *hot(R).cld(X) = cld(R).hot(X)*.
Also, each of the functions is a sine wave, we can assume that the *eqx()*
value is the average of *cld()* and *hot()* values.  Specifically,

> *eqx(R).eqx(X)
> = (cld(R)+hot(R))/2 . (cld(X)+hot(X))/2
> = cld(R).cld(X)/2 + cld(R).hot(X)/2 + hot(R).cld(X)/2 + hot(R).hot(X)/2
> = cld(R).cld(X) + cld(R).hot(X)*

To summarise, we now have the following saddle point computations:

  * Cold Solstice: *cld(D) + cld(R).cld(X)*
  * Hot Solstice:  *hot(D) + cld(R).cld(X)*
  * Equinoxes:     *(cld(D) + hot(D))/2 + cld(R).cld(X) + cld(R).hot(X)*

Note that the Equinox formula involves the average of the Cold and Hot
Solstices plus *cld(R).hot(X)*.

The equations have 4 degrees of freedom, if we treat *cld(R).cld(X)* and
*cld(R).hot(X)* each as a degree of freedom.  Since we have three saddle
points to match, this implies a degree of freedom, which may help match
the situation to technical constraints; it gives us some wiggling room.
We can play with the difference between direct light and reflected light
to find actual values for offsets and amplitudes of *D*, *R* and *X*.
The phase of these sine waves is dictated by the Sun.

The freedom can be used to transport Sun capturing between direct light *D*
and reflection *R*, but when starting a model, this is mostly a nuisance.
So we can simplify the problem somewhat with initial choices:

  * *cld(X) = 1.0* — initially use full reflection under the Cold Solstice
  * *hot(X) = 0.0* — initially use no   reflection under the Hot Solstice
  * Later change this when it seems practical


TODO: MAP D/R/X DIMENSIONS TO HEAT PIPE ANGLE AND TO REFLECTOR ANGLE AND SIZE.

