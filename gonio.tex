\section{Reflector hanging over Heat Pipes}

The first design proposal uses solar heat pipes that lay relatively
flat, with a reflector mounted over them.  When the Sun is high,
the same reflector will cast shade over the heat pipes, avoiding
superfluous heat that may result in boiling heat pipe fluid.  To
avoid reflections that may blind people around the building, it is
best to paint the reflector black on top; its local heating-up is
not likely to cause problems.

\begin{figure}[hbt]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig1-design.pdf}
\end{center}
\caption{The assumed parameters for a reflector hanging over heat pipes; $\alpha$ is the heat pipe angle, $\beta$ is the reflector angle, $\varphi$ is the Solar incidence angle.}
\end{figure}

The area that receives Sunlight depends on the various angles, and
is a measure of how efficient the heat pipes are being used.  The
fact that heat pipes let some radiation pass through is not taken
into account, so a reflector underneath them is not either.

\subsection{Direct Area without Shading}

We can determine the area that captures direct light when the
reflect casts no shade, so when $\varphi\leq\beta$.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig2-direct.pdf}
\end{center}
\caption{Direct Area without shading cast by the reflector, $\varphi\leq\beta$.}
\end{figure}

In this case, the area is a part of the heat pipe area $A_0$:

\begin{eqnarray}
A_\textrm{direct} &=& A_0\cdot \cos (90\degrees-\alpha-\varphi)\\
	&=& A_0\cdot \sin\,( \alpha+\varphi )
\end{eqnarray}

\subsection{Reflected Area}

\textbf{TODO:BUG:} Pieter-Tjerk de Boer points out that the
reflected area should be used without projection on the
heat pipes.  I add that the part that arrives on heat pipes
should be used, but this may be less if part shines outside
the heat pipe area.  The result is probably a min-or-max.

The reflector casts back the Sun over its full length, which is
assumed to match that of the heat pipes.  The reflection may
be of a larger or smaller size than that of the heat pipes.

An alternative way of considering this is that the heat pipes
are reflected in the reflector, and capture extra light over a
heat pipe length limited by the reflector length under given
angles.  Note that the reflection is towards the hot end of the
heat pipe, namely its high point.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig3-reflected.pdf}
\end{center}
\caption{Reflected Area considered as heat pipes mirrorred by the reflector, $\varphi\leq\beta$.}
\end{figure}

First, we need to infer the size of the heat pipe mirror image,
and then we shall project it on the plane orthogonal to the
Sun's light.

The reflector is assumed to have area $A_0$ and the mirror image
will be given the symbol $A_\textrm{mirage}$ for its area:

\begin{eqnarray}
A_0 \over \sin\,(180\degrees - 2\beta - \alpha + \varphi) &=& A_\textrm{mirage} \over \sin\,(\beta-\varphi)\\
A_\textrm{mirage} &=& A_0 \cdot { \sin\,( \beta-\varphi ) \over \sin\,( 180\degrees -2\beta - \alpha + \varphi) }\\
	&=& A_0 \cdot { \sin\,( \beta - \varphi) \over \sin\,(\alpha + 2\beta - \varphi)}
\end{eqnarray}

Based on this, the mirror image can be reduced to the reflected
image, for which the symbol $A_\textrm{reflect}$ will be used:

\begin{eqnarray}
A_\textrm{reflect} &=& A_\textrm{mirage} \cdot \cos ((90\degrees - \beta - \varphi) - (\alpha + \beta)) \\
	&=& A_\textrm{mirage} \cdot \sin\,(\alpha + 2\beta + \varphi)\\
	&=& A_0 \cdot { \sin\,( \beta - \varphi ) \cdot \sin\,(\alpha + 2\beta + \varphi) \over \sin\,(\alpha + 2\beta - \varphi) } \\
	% sin A . sin B = [ cos ( A - B ) - cos ( A + B ) ] / 2
	% sin F-A-2B . sin F+A+2B = [ cos -2A-4B - cos 2F ] / 2
	%%%TODO%PROBABLY%NOT%%% &=& A_0 \cdot { \cos (2\alpha+4\beta) - \cos (2\varphi) \over 2 \sin\,( \beta - \varphi ) }
\end{eqnarray}


\subsection{Shaded Area}

When the Sun is at a high position, $\varphi>=\beta$, then the reflector casts
a shade onto the heat pipes.  The top side of the reflector should not be
reflective (black is a good choice) so it does not bounce off into the
surroundings where it might blind people.

In this situation, there would be no reflection on the heat pipes but only
a shaded area; the shading is good, because it helps to prevent overheating
of the heat pipes in a time of year when their energy is not required.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig4-direct-shaded.pdf}
\end{center}
\caption{Direct but Shaded Area, due to a shadow cast by the reflector, $\varphi\geq\beta$.}
\end{figure}

The shaded area, given the symbol $A_\textrm{shaded}$, will first be derived from the reflector
area $A_0$, followed by the remaining area $A_\textrm{remain}$.

\begin{eqnarray}
A_\textrm{shaded}\over \sin\,(\varphi-\beta) &=& A_0 \over \sin\,(180\degrees - \alpha - \varphi )\\
A_\textrm{shaded} &=& A_0 \cdot { \sin\,(\varphi-\beta) \over \sin\,(180\degrees - \alpha - \varphi ) } \\
	&=& A_0 \cdot { \sin\,(\varphi-\beta) \over \sin\,(\varphi+\alpha) }
\end{eqnarray}

The non-shaded area needs to projected on the plane orthogonal to the beam from
the Sun to find $A_\textrm{remain}$,

\begin{eqnarray}
A_\textrm{remain} &=& ( A_0 - A_\textrm{shaded} ) \cdot \cos (90\degrees - \varphi - \alpha) \\
	&=& (A_0 - A_\textrm{shaded} ) \cdot \sin\,(\varphi + \alpha) \\
	&=& A_0 \cdot \sin\,(\varphi + \alpha) - A_0 \cdot \sin\,(\varphi-\beta)
\end{eqnarray}


\subsection{Total Efficiency}

The total area that captures the Sun is described as the area perpendicular to
its light and heat rays.  It may be smaller as a result of non-orthognal
heat pipes; it may be higher as a result of reflection.  The effective area of
the heat pipes and possible reflection underneath is not counted herein.
The efficiency is the effective area as a fraction of the heat pipe area.

\begin{eqnarray}
\eta &=& \begin{cases}
		\displaystyle
		{A_\textrm{direct} + A_\textrm{reflect} \over A_0},&\textrm{if $\varphi\leq\beta$}\\
		\displaystyle
		{A_\textrm{remain} \over A_0},&\textrm{if $\varphi\geq\beta$}
	\end{cases}
\end{eqnarray}

This area depends on static configuration parameters $\alpha$ and $\beta$, and
on the variable Sun angle $\varphi$ above the horizon.

%ODS% \subsection{Concrete Examples}
%ODS% 
%ODS% Assuming $\alpha=25\degrees$ and $\beta=42\degrees$, we find:
%ODS% \begin{itemize}
%ODS% \item When $\varphi=14\degrees$ we have $\eta=\sin\,(25\degrees+14\degrees)+{\sin\,(42\degrees-14\degrees)\cdot\sin\,(25\degrees+2\cdot 42\degrees+14\degrees)\over\sin\,(25\degrees+2\cdot 42\degrees-14\degrees)}\approx 63\%+20\%=83\%$
%ODS% \item When $\varphi=63\degrees$ we have $\eta=\sin\,(63\degrees+25\degrees) - \sin\,(63\degrees-42\degrees)\approx 64\%$
%ODS% \item When $\varphi=\beta=42\degrees$ we have $\eta=\sin\,(42\degrees+25\degrees)\approx 92\%$
%ODS% \end{itemize}


%SAVINGPAPER% \clearpage
\section{Reflector in front of Heat Pipes}

The second design proposal assumes the heat pipes are more vertically
positioned, with a reflector in front.  Heat pipes are slotted and capture
less wind than the highly mounted solid reflector in the first design.
The higher the position of the Sun, the less light would be caught directly.
The reflector can be used to enlarge the area, especially when the Sun is low,
that is exposed to light, especially when the reflector is copiously sized.

\begin{figure}[hbt]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig5-design.pdf}
\end{center}
\caption{The assumed parameters for a reflector before heat pipes; $\alpha$ is the heat pipe angle, $\beta$ is the reflector angle, $\varphi$ is the Solar incidence angle.}
\end{figure}

\subsection{Direct Area}

It is not productive to make the reflector cast a shade, because that degrades
performance when the Sun is low.  We therefore assume that all light is caught
directly.  What may however happen is that the Sun rises so high that it
crosses over the the back of the heat pipes (when $\alpha>90\degrees$).
The direct area computed here does not assume that this happens, so the assumption
is $\alpha+\varphi\leq 180\degrees$.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig6-direct.pdf}
\end{center}
\caption{Direct Area on standing heat pipes, $\varphi\leq 180\degrees-\alpha$.}
\end{figure}

The direct area needs to projected on the plane orthogonal to the beam from
the Sun to find $A_\textrm{remain}$,

\begin{eqnarray}
A_\textrm{direct} &=& A_0 \cdot \cos (90\degrees - \varphi - \alpha) \\
	&=& A_0 \cdot \sin\,(\varphi + \alpha)
\end{eqnarray}

\subsection{Reflected Area}

Since it is not productive to make the reflector cast a shade, we shall
assume that $\beta<\varphi$, at least on the (most influential) position
with the Sun is at its mid-day position.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig7-reflected.pdf}
\end{center}
\caption{Reflected Area on standing heat pipes, $\varphi\geq\beta$.}
\end{figure}

The reflected area is caused by incoming radiation at the reflector,
whose area is assumed to be $A_1$ and it is generally attractive
when $A_1>A_0$, but not required.  Heat pipes are impartial about
where the light lands, or under what angle, but the reflected area
is only useful up to effective heat pipe area $A_\textrm{pipe}$.

\begin{eqnarray}
A_\textrm{full} &=& A_1 \cdot \sin\,(\varphi - \beta)\\
A_\textrm{pipe} &=&
	\begin{cases}
		\displaystyle
		A_0 \cdot \sin\,(2\beta+\alpha-\varphi),&\textrm{if $2\beta+\alpha-\varphi\geq 0\degrees$}\\
		A_0 \cdot 0,&\textrm{if $2\beta+\alpha-\varphi\leq 0\degrees$}
	\end{cases}\\
	&=&
	A_0 \cdot \max\,(0\,,\,\sin\,(2\beta+\alpha-\varphi))\\
A_\textrm{reflect} &=& \min\,(A_\textrm{full}\,,\,A_\textrm{pipe})
\end{eqnarray}

\textbf{Fresnel reflectors.}
A reflector with ridges may be used when $\beta<0\degrees$, so that the
heat pipes need not be raised (relative to a flat roof).  The ridges
may however cast a shadow on the reflector area, effectively causing
strips in the light falling on the reflector.   Those stripes do not
appear on the heat pipe, but they reduce the height that lights up due
to reflection.  To compensate, the size of the reflector may be enlarged
by dividing it with the striping ratio.

Guiding the low position of the Sun with a $\delta$-slanted Fresnel drop
is not beneficial, as this reflector area adds nothing for low positions
but it may cause reflection onto the heat pipes during high positions,
which is undesirable.  An easy choice is $\delta=90\degrees$, but with
care for the high Sun position or blackening of the drop, a reduced
height of the reflector may be achievable.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig8-fresnel.pdf}
\end{center}
\caption{Shadow cast on Fresnel reflector, $\beta<0\degrees$.}
\end{figure}

A more detailed analysis focusses on a single triangle in the Fresnel
reflector, and determines how the horizontal size $F$ should be to have
a given size $L$ that reflects light.  These conditions depend on the
angle $\beta<0$ and the angle $\varphi$ of incoming light.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig9-fresnel-ratios.pdf}
\end{center}
\caption{Horizontal size $F$ for a Fresnel reflector sized $L$, $\beta<0\degrees$ and $\varphi\leq\delta$.}
\end{figure}

The ratio between $F$ and $L$ can be determined with the sine rule,

\begin{eqnarray}
F\over L &=& \sin\,(180\degrees+\beta-\varphi)\over \sin\,(\varphi) \\
	&=& -\sin\,(\beta-\varphi) \over \sin\,(\varphi) \\
	&=& \sin\,(\varphi-\beta) \over \sin\,(\varphi)
\end{eqnarray}

The size $A_1$ for a non-Fresnel reflector would have a horizontal
size $A_1\cdot \cos\,(\beta)$, but for a Fresnel reflector we would
use $A_1\cdot F/L$ instead.  Let's derive the horizontal size for
$A_1$ and call it $A_2$:

\begin{eqnarray}
A_2 &=& \begin{cases}
		\displaystyle
		A_1 \cdot \cos (\beta),&\textrm{for non-Fresnel and $\beta\geq 0\degrees$}\\
		\displaystyle
		A_1 \cdot {\sin\,(\varphi-\beta) \over \sin\,(\varphi)},&\textrm{for Fresnel and $\beta\leq 0\degrees$}
	\end{cases}
\end{eqnarray}

Note that there is no use for the reflective area below $L$, at least
for the highest possible position of the Sun, so this area may be
carved out less deeply by making it more slanted.  It is also possible
to use that $\varphi>\delta$ causes striping on the heat pipes, which
can further improve the ratio between low and high Sun positions.
This mechanism effectively `clips' the heat captured, and this may be
used to remedy high-position overheating without affecting low positions;
black tape could be retrofitted in troughs of a Fresnel reflector.

\paragraph{Need for Refinement.}
The figure and computation suggest that the slope $\beta$ is fixed
over the entire Fresnel reflector, but in reality that would draw a
striping pattern on the heat pipes.  This is most likely a problem, so
there is a need to adapt $\beta$ such that each Fresnel portion reaches
the heat pipes.

When $\beta$ is recalculated for every Fresnel segment then it will
flatten out for segments more remote from the heat pipes.  A result of
this is a rise in the $F/L$ ratio.  At some point, a flat area will
suffice, and $F/L=1$ avoids further disruptions that introduce the
need for a larger surface.

The need to refine $\beta$ through local variation enables a degree of
control over the highest $\varphi$ that reflects from any given Fresnel
segment; this idea is worked out in detail in the refining section below.


\subsection{Refined Reflectors}

After the coarse treatment of Fresnel reflectors above, the treatment
can now be refined into a differential treatment.  In this subsection
no attention will be paid to Fresnel ridges, but rather a smooth
differential model is derived for a reflector that is assumed to
somehow fit into the physical space.  This approach allows a high degree
of control can be had over the reflection in response to any slope
$\varphi$ of the Sun.

Reflectors are best used to compensate for the seasonal defaults, where
a high Sun position yields most heat and low Sun positions yield the
least.  What we usually desire is the opposite, because the Sun position
already imparts the heat onto the surroundings of the heat pipe system.
(This could change when heat can be efficiently stored in the hot season
for use in the cold season.)

The model proposed here is a differential model with indepent variable
$x$ being the horizontal distance in South direction from the place where
heat pipes and reflector touch.  A slightly more accurate model might
add a variable $y$ as the vertical distance, but this is not done here
because it does not seem to reap much benefit.

\paragraph{Reflection requirement.}
Define $R(\varphi)$ as the amount of light to reflect when it enters
under slope $\varphi$.  This value defines a desire (or requirement)
in addition to the direct light hitting the heat pipes.  The value
of $R(\varphi)$ will usually fall for higher $\varphi$ values, but
this is not a requirement.

\paragraph{Conditional Helper.}
We need to integrate below, using value $1$ for truth and $0$ otherwise.
\begin{eqnarray}
O(b) &=&
	\begin{cases}
		\displaystyle
		{1},&\textrm{if $\phantom{\neg }b$}\\
		\displaystyle
		{0},&\textrm{if $\neg b$}
	\end{cases}
\end{eqnarray}

\paragraph{Positional reflection.}
Define $\hat\varphi(x)$ as the highest slope $\varphi$ that will be
reflected onto the heat pipes from position $x$.  Light from this
slope would be directed to the top end of the heat pipes, so any
more vertical slopes would reflect before the heat pipes.

Assuming that $\hat\varphi(x)$ drops for higher values of $x$, that is,
assuming that the higher slopes $\varphi$ are reflected from nearby
the heat pipes, we can relate $R(\varphi)$ and $\hat\varphi(x)$ as
follows:

\begin{eqnarray}
\hat\varphi(x) &=& \int_{\varphi=0\degrees}^{90\degrees} O(\,R(\varphi)\,) \;d\varphi\\
R(\varphi) &=& \max\,\{\, x \,|\, \hat\varphi(x) \geq \varphi \,\}
\end{eqnarray}

\paragraph{Top of heat pipes.}
The position of the top of the heat pipes is at position $x_T$ in the
same dimension as $x$.  The length of the heat pipes is assumed to
be $2m$, and their angle is $\alpha$.  This allows the following
definition:

\begin{eqnarray}
x_T &=& 2m \cdot \sin\,(\alpha-90\degrees)\\
	&=& -2m\cdot\sin\,(90\degrees-\alpha)\\
	&=& -2m\cdot\cos\,(\alpha)
\end{eqnarray}

\paragraph{Aim for the top.}
From position $x,y$, with $x$ as horizontal distance from the heat pipe base in South direction
and $y$ the height above the heat pipe base, the angle $\hat\tau(x,y)$ at which the top can be
found is:

\begin{eqnarray}
\hat\tau(x,y) &=& \arctan\left({2m\cdot\sin\,(\alpha)-y \over x-x_T}\right)\\
	&=& \arctan\,\left({2m\cdot\sin\,(\alpha)-y \over x + 2m\cdot\cos\,(\alpha)}\right)
\end{eqnarray}

It may often be reasonable to assume $y=0$ as an approximation, but $x$ is vital.
Note that these are the start positions of the reflector, so no recursive
computations are required when reflector segments are derived starting at the heat
pipe base (which is also required for the computational dependency on $x$).

\paragraph{Reflector slope.}
To reflect light at slopes $\hat\varphi(x)$ to the top of the heat pipes in
direction $\hat\tau(x,y)$, we need the following value for the slope of the
reflector at position $x$:

\begin{eqnarray}
\hat\beta(x,y) &=& { \hat\varphi(x) + \hat\tau(x,y) \over 2 }
\end{eqnarray}

Usually, $\hat\beta(x)\leq 0$.
This fully defines the reflector, which starts at the base of the heat pipes
and takes on slope $\hat\beta(x)$ for increasing values of $x$.


\subsection{Refined Fresnel Reflectors}

The definition of Refined Reflectors assumes that it fits in the physical reality,
and is not stopped by such interferences as a roof surface.  In places where
the physical reality is not so foregiving, a Refined Fresnel Reflector may
be used to establish the same result, at the cost of a longer horizontal size.

\paragraph{Roof slope.}
Let $\gamma$ be the slope by which the roof rises while moving towards the
South.  A flat roof has $\gamma=0$ and a roof that drops has $\gamma<0$.
The angle is assumed to be fixed (but for Japanese buildings we could
generalise to $\hat\gamma(x)$).

\paragraph{Reflector dive.}
When the depth at a position $x$ of the reflector goes below the roof slope,
it is necessary to introduce a Fresnel ridge.  This happens at any position
$x$ where

\begin{eqnarray}
x\cdot\tan\,(\gamma) &>& \int_{z=0}^x \hat\beta(z) \;dz
\end{eqnarray}

\paragraph{Ridges.}
A ridge is a part of the function where $\hat\beta(x)$ rises to stay above the
roof slope $x\cdot\tan\,(\gamma)$.  It is a matter of taste (and need) how far to rise.
Generally, the angle is supposed to be $\hat\varphi(x)$.

Note that a dropping ridge is also possible, by using $\hat\beta(x,y)=\hat\tau(x,y)$
over a width of $x$.  Generally, active areas between ridges adhere to
$x\cdot \tan\,(\gamma) < \hat\beta(x,y) < \hat\tau(x,y)$.

Ridges must be discounted in the definition of $R(\varphi)$ by subtracting from
the originally computed value of $x$ those widths of $x$ where $x\cdot\tan\,(\gamma)\geq\hat\beta(x,y)$
or $\hat\beta(x,y)\geq\hat\tau(x,y)$.
Likewise, the definition of $\hat\varphi(x)$ must discount any such widths inasfar
as the from $0$ and $x$.

\begin{eqnarray}
R(\varphi) &=& \max\,\{\, x \,|\, \hat\varphi(x) > \varphi \,\} - \int_{z=0}^x O(\,x\cdot\tan\,(\gamma) \geq \hat\beta(x,y) \mathop{\vee} \hat\beta(x,y) \geq \hat\tau(x,y)\,) \;dz
\end{eqnarray}

\paragraph{Flatliner.}
For some high-enough value of $x$, the value of $\hat\beta(x,y)$ rises to $x\cdot \tan\,(\gamma)$
or it may even aim to be higher.  This is the place where the reflector can stop
producing Fresnel ridges, and it can continue as a flat line, parallel to the
roof.  To accommodate being at the plane of the roof, the last Fresnel ridge
may be held low, or an opposite ridge could be made, using $\hat\tau(x,y)$ as
guidance.

\paragraph{Elongation.}
The refined Fresnel reflector tends to be longer than the original definition
of a refined reflector.  The extra length is the area where $x\cdot\tan\,(\gamma)\geq\hat\beta(x,y)$
or $\hat\beta(x,y)\geq\hat\tau(x,y)$.


\subsection{Total Efficiency}

The total area that captures the Sun is described as the area perpendicular to
its light and heat rays.  It may be smaller as a result of non-orthognal
heat pipes; it may be higher as a result of reflection.  The effective area of
the heat pipes and possible reflection underneath is not counted herein.
The efficiency is the effective area as a fraction of the heat pipe area.

\begin{eqnarray}
\eta &=& %\begin{cases}
		%\displaystyle
		{A_\textrm{direct} + A_\textrm{reflect} \over A_0},\textrm{ if $\beta\leq\varphi\leq 180\degrees-\alpha$}
	%\end{cases}
\end{eqnarray}

This area depends on static configuration parameters $\alpha$ and $\beta$, and
on the variable Sun angle $\varphi$ above the horizon.


%ODS% \subsection{Concrete Examples}
%ODS% 
%ODS% Assuming $\alpha=45\degrees$ and $\beta=0\degrees$, we find:
%ODS% \begin{itemize}
%ODS% \item When $\varphi=14\degrees$ we have $\eta=TODO:\sin\,(25\degrees+14\degrees)+{\sin\,(42\degrees-14\degrees)\cdot\sin\,(25\degrees+2\cdot 42\degrees+14\degrees)\over\sin\,(25\degrees+2\cdot 42\degrees-14\degrees)}\approx 63\%+20\%=83\%$
%ODS% \item When $\varphi=63\degrees$ we have $\eta=TODO:\sin\,(63\degrees+25\degrees) - \sin\,(63\degrees-42\degrees)\approx 64\%$
%ODS% \item When $\varphi=\beta=42\degrees$ we have $\eta=TODO:\sin\,(42\degrees+25\degrees)\approx 92\%$
%ODS% \end{itemize}
%ODS% 


\section{Reflecting Blinds before Heat Pipes}

Yet another possible configuration is to place blinds in front of the heat pipes.
These blinds are assumed to be reflective on both sides, so there is no shadow
from them; since blinds would warm up when lighted, they are bound to radiate
such heat, so reflection on both sides is the most controlling form.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.2]{zigzagzon-fig10-blinds-design.pdf}
\end{center}
\caption{Design with two-sided Reflective Blinds before Heat Pipes; Left shows four possible light paths; right top shows light angles and right bottom shows construction angles $\epsilon_1<\epsilon_2<90\degrees<\epsilon_3$.}
\end{figure}

Depending on the angle $\varphi$ of incoming light from the Sun, the
response can be (a mixture of) four possible paths:
\begin{itemize}
\item\emph{Direct}
	is the passing of light between blinds, to directly hit the
	heat pipes; the efficiency is $\eta_d=1$;
\item\emph{Mirror Once}
	is the upside-down projection of the light after one bounce
	on a blind; the efficiency is a parameter $\eta_m$;
\item\emph{Mirror Twice}
	is the projection of the light after two bounces on two blinds;
	the efficiency is $\eta_m^2$;
\item\emph{Reject}
	is the rejection of the light by sending it back to the sky;
	the heat is not captured and does not add to climate heating;
	the efficiency is $\eta_r=0$.
\end{itemize}
In general, the interesting measurement is the area of light from the
Sun, measured orthogonally to the Sun, that is given each of these
treatments.  Each treatment has its own efficiency, but only $\eta_m$
introduces a new independent parameter.  These efficiency factors will
be used to reduce the area that is passed along these light paths.

The blinds have a particular length $L$ and their distance along the
heat pipes is $D$.  The ratio $D{:}L$ is useful to parameterise the
model, but when $L$ and $D$ are divided by the same factor then the
only result is that the light paths are sliced in more places, but
in the same manner.  Therefore, their ratio is the only parameter
used to represent both choices.

\subsection{Blinds' Constraint Angles}

TODO: These constraints do not seem to be useful to derive areas!

A few angles $\epsilon_1$, $\epsilon_2$ and $\epsilon_3$ are first
derived from the parameters $\alpha$, $\beta$ and $D{:}L$.
These can be used to distinguish the light path taken and from that
compute effective light captured; they constrain incident light to
certain paths.

We shall first derive the vertical size $V$ and horizontal size $H$
for the parallellogram between two consecutive blinds,
\begin{eqnarray}
V
	&=& D\cdot\sin\,(\alpha) + L\cdot\sin\,(-\beta)\\
	&=& D\cdot\sin\,(\alpha) - L\cdot\sin\,(\beta)\\
H
	&=& D\cdot\cos\,(\alpha) + L\cdot\cos\,(-\beta)\\
	&=& D\cdot\cos\,(\alpha) + L\cdot\cos\,(\beta)
\end{eqnarray}
and so,
\begin{eqnarray}
V:L
	&=& D{:}L \cdot\sin\,(\alpha) - \sin\,(\beta)\\
H:L
	&=& D{:}L \cdot\cos\,(\alpha) + \cos\,(\beta)\\
\end{eqnarray}

Based on this, we can derive the constraint angles:
\begin{eqnarray}
\epsilon_1
	&=& \arctan\,(V~/~H)\\
	&=& \arctan\,(V{:}L~/~H{:}L)\\
	&=& \arctan\,\left({ D{:}L \cdot\sin\,(\alpha) - \sin\,(\beta) \over D{:}L \cdot\cos\,(\alpha) + \cos\,(\beta) }\right)\\
\epsilon_2
	&=& \alpha\\
\epsilon_3
	&=& TODO
\end{eqnarray}

% We start off in the bottom triangle to determine $D'$ and $L'$,
% \begin{eqnarray}
% D':L
% 	&=& \sin\,(-\beta) \over \sin\,(180\degrees-\alpha)\\
% 	&=& \sin\,(\beta) \over \sin\,(\alpha)\\
% L':L
% 	&=& \sin\,(-\beta) \over \sin\,(180\degrees+\alpha+\beta)\\
% 	&=& \sin\,(\beta) \over \sin\,(\alpha+\beta)
% \end{eqnarray}


% We will also project from L straight down, to find $D''$ and $L''$
% values (not in the figure):
% \begin{eqnarray}
% D''
% 	&=& L \cdot \sin\,(-\beta) \\
% 	&=& -L \cdot \sin\,(\beta) \\
% L''
% 	&=& L \cdot \cos\,(-\beta) \\
% 	&=& L \cdot \cos\,(\beta)
% \end{eqnarray}
% TODO: Not directly usable to derive unknown variables.


% Now we can extend by adding the triangle resting on top of it,
% between angles $\epsilon_1$ and $\alpha$ to derive the former,
% \begin{eqnarray}
% (D+D'):L'
% 	&=& \sin\,(180\degrees-\epsilon_1-(180\degrees-\alpha) \over \sin\,(\epsilon_1)\\
% 	&=& \sin\,(\alpha-\epsilon_1) \over \sin\,(\epsilon_1)
% \end{eqnarray}
% TODO: Cannot isolate $\epsilon_1$ from this...


\subsection{Direct Light}

The direct path has perfect efficiency $\eta_d=1$.
Some of the area is reduced if the slope $\varphi$ of incident light is not orthogonal to the heat pipe slope $\alpha$ (naturally measured, meaning in opposite directions).

The slope-corrected light that is offered to the assembly of blinds and heat pipes is
\begin{eqnarray}
A_\textrm{offer} &=& A_0 \cdot \cos\,(90\degrees - \varphi - \alpha) \\
	&=& A_0 \cdot \sin\,(\varphi + \alpha)
\end{eqnarray}
This will be used in the other derivations as well.

For light coming in under angle $\varphi$, the blinds appear to have a height, say $L_\varphi$,
and the distance between blinds appears to have a height, say $D_\varphi$:
\begin{eqnarray}
L_\varphi:L
	&=& \sin\,(\varphi - \beta) \\
D_\varphi:L
	&=& D{:}L \cdot \sin\,(\varphi + \alpha)
\end{eqnarray}

A concern with these formul\ae\ is that two blinds might get behind each other
when $D_\varphi>L_\varphi$, so we need to introduce a boundary when deriving
the effective direct area:
\begin{eqnarray}
A_\textrm{direct}
	&=& A_\textrm{offer}\cdot \max\,\left(0,L_\varphi:D_\varphi\right)\cdot\eta_d\\
	&=& A_\textrm{offer}\cdot \max\,\left(0,{L_\varphi:L\over D_\varphi:L}\right)\cdot 1\\
	&=& A_\textrm{offer}\cdot \max\,\left(0,D{:}L\cdot{\sin\,(\varphi+\alpha)\over\sin\,(\varphi-\beta)}\right) \\
	&=& A_\textrm{offer}\cdot D{:}L\cdot \max\,\left(0,{\sin\,(\varphi+\alpha)\over\sin\,(\varphi-\beta)}\right)
\end{eqnarray}

The part reflected is $\eta'_d=1-\eta_d=0$.


\subsection{Rejected Light}

Light is rejected (that is, reflected into oblivia, or usually, back into space
to avoid heating up the Earth) and so its efficiency $\eta_r=0$ for heat capture
is absent.  We do compute the amount of light reflected because it is a measure
of how much better the solution is from a climate-heating perspective.  For that,
we could define $\eta'$ or, in this case, $\eta'_r=1-\eta_r=1$.
TODO: We might add such a computation elsewhere too.

To be rejected, a blind must bounce the incoming light under slope $\varphi$ in a
position before the blind right above it.  The bounced light will be under angle
$\varphi-2\beta$, and one blind's tip can only escape the tip of the blind above
it when $\varphi-2\beta>\alpha$ or $\varphi>\alpha+2\beta$.

Projecting the blind above on the blind below, we find $L_\textrm{blprj}$, and
projecting under $\varphi-2\beta$ we find the length $L_\textrm{blbl}$ to subtract
to find the rejected blind length $L_\textrm{blrej}$, which subsequently leads to
the area $A_\textrm{reject}$ of incident light reflected:
\begin{eqnarray}
L_\textrm{blprj}
	&=& D\cdot \cos\,(\alpha+\beta)\\
	&=& L\cdot D{:}L \cdot \cos\,(\alpha+\beta)\\
L_\textrm{blbl}
	&=& D\cdot \cos\,(\varphi-\beta)\\
	&=& L\cdot D{:}L \cdot \cos\,(\varphi-\beta)\\
L_\textrm{blrej}
	&=& \max\,(0,L_\textrm{blprj} - \textrm{blbl}) \\
	&=& L\cdot D{:}L \cdot \max\,(0,~\cos\,(\alpha+\beta) - \cos\,(\varphi-\beta)~) \\
L_\textrm{reject}
	&=& L_\textrm{blrej} \cdot \sin\,(\varphi-\beta) \\
	&=& L\cdot D{:}L\cdot \max\,(0,~\cos\,(\alpha+\beta) - \cos\,(\varphi-\beta)~)\cdot \sin\,(\varphi-\beta)
\end{eqnarray}
This length can be stretched over the width of the heat pipes and blinds:
\begin{eqnarray}
A_\textrm{reject}
	&=& A_\textrm{offer} \cdot L_\textrm{reject}{:} L\\
	&=& A_\textrm{offer} \cdot D{:}L\cdot \max\,(0,~\cos\,(\alpha+\beta) - \cos\,(\varphi-\beta)~)\cdot \sin\,(\varphi-\beta)
\end{eqnarray}
The part reflected is $\eta'_r$ and is relative to the offered area:
\begin{eqnarray}
\eta_r'
	&=& A_\textrm{reject} : A_\textrm{offer} \\
	&=& A_\textrm{offer} \cdot D{:}L\cdot \max\,(0,~\cos\,(\alpha+\beta) - \cos\,(\varphi-\beta)~)\cdot \sin\,(\varphi-\beta)
		\over A_\textrm{offer} \\
	&=& D{:}L \cdot \max\,(0,~\cos\,(\alpha+\beta) - \cos\,(\varphi-\beta)~)\cdot \sin\,(\varphi-\beta)
\end{eqnarray}


\subsection{Mirrorred Light}

Light can pass between two blinds, above that it is possible for light that
hits the tip of a blind to reject that light.  Any other light is mirrorred
by the first blind, and may subsequently be reflected back by the one above it.
Mirrorring once has efficiency parameter $\eta_m$ but mirrorring may occur
$i$ times, leading to efficiency $\eta_m^i$.

The area offered for mirrorring is what remains after direct and rejected light, so
\begin{eqnarray}
A_\textrm{mirror}
	&=& A_\textrm{offer} - A_\textrm{direct} - A_\textrm{reject} %TOOLONG% \\
	%TOOLONG% &=& A_\textrm{offer} \cdot D{:}L \cdot \left( 1 - \max\,(0, {\sin\,(\varphi+\alpha)\over\sin\,(\varphi-\beta)} - TODO \right)
\end{eqnarray}

TODO: Derive mirrorring $i$ times.

Non-reflected light must be considered as converted to stray heat, so it adds
to climate warming and will not reflect back to space as light.
Because of this, $\eta_m'=0$ for any number of times $i$ that mirrorring occurrs.


\section{Conclusions}

Heat pipes and a reflector can be combined in a number of ways, and various angles.
The formul\ae\ described herein offer opportunities for optimisation in any
particular setting, especially on a flat roof.

It is possible to make the heat pipes lean over towards the Sun, such that it
barely captures light at its highest position.  This can help to reduce the
output in that position, which enables more heat pipes without boiling transport
liquid, and capture more heat in lower Sun positions.

In terms of catching wind it is more attractive to mount the solid reflector below
the slotted collector with heat pipes.  In situations with the extra space for a
reflector with Fresnel ridges, it is possible to `burry' the reflector below the
flat surface of the roof.


\section{Further Work}

The formul\ae\ presented herein assume direct light, while in reality clouds
may cause scattered light.  Heat pipes perform well in such conditions, but
the orientation angle $\alpha$ may be of influence on the performance of a different
tilt.  This should be determined, especially for the lower Sun positions.
During low Sun, it is probably beneficial when the heat pipes are more
vertically positioned, so they have a better outlook on sunrise and sunset.

The formul\ae presented herein only consider mid-day.  A full model would take
account of the rotation of the Sun and slanted light beams, including its
impact on reflection.  A full model would incorporate the motion of the Sun
in a circle, and it would cut off invisible parts of that motion.  This may
be kept sufficiently general to accommodate regions outside the polar circles.

To combine the reflector hanging over the heat pipes with one lying in front
of the heat pipes, it makes sense to consider blind-shaped reflectors hung
directly in front of the heat pipes.  These may reflect light to the heat pipes
as well as away from them, and this would be heavily dependent on the angle
of incident light.


\section{Acknowledgements}

Thanks to Hans Mulder for suggesting that light can be passed back out to space,
unlike the captivated heat that results from most energy transformations.

Thanks to Pieter-Tjerk de Boer for pointing out an error in the gonio of the
reflector in Figure 3.
