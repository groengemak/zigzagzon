#!/usr/bin/env python3
#
# Statistical analysis of CSV data from KNMI
#
# From: Rick van Rein <rick@groengemak.nl>


import sys
import csv
import datetime
from statistics import NormalDist


#
# Parse arguments
#
prefstn = 290


#
# Construct date ranges for the analysis
#
sampleyears = range (2013,2023)
#
sampledates = [
	(21,12,-1), (30,12,-1),
	(8,1,0),(17,1,0),(26,1,0),
	(4,2,0),(13,2,0),(22,2,0),
	(3,3,0),(12,3,0),(21,3,0),(30,3,0),
	(8,4,0),(17,4,0),(26,4,0),
	(5,5,0),(14,5,0),(23,5,0),
	(3,6,0),(12,6,0),(21,6,0),(30,6,0),
	(9,7,0),(20,7,0),(29,7,0),
	(8,8,0),(17,8,0),(26,8,0),
	(4,9,0),(13,9,0),(22,9,0),
	(1,10,0),(10,10,0),(19,10,0),(28,10,0),
	(6,11,0),(15,11,0),(24,11,0),
	(3,12,0),(12,12,0)
]
#
samplekeys = [
	'%04d-%02d-%02d' % (yyyy+yo,mm,dd)
	for yyyy in sampleyears
	for (dd,mm,yo) in sampledates
]
#DEBUG# print ('samplekeys = %s,%s,%s,%s,%s,...' % tuple (samplekeys [:5]), file=sys.stderr)


#
#KNMIstationslist:numbertoname
#
stn2stnm={
	209: "IJmond",
	210: "ValkenburgZh",
	215: "Voorschoten",
	225: "IJmuiden",
	229: "Texelhors",
	235: "DeKooy",
	240: "Schiphol",
	242: "Vlieland",
	248: "Wijdenes",
	249: "Berkhout",
	251: "HoornTerschelling",
	257: "WijkaanZee",
	258: "Houtribdijk",
	260: "DeBilt",
	265: "Soesterberg",
	267: "Stavoren",
	269: "Lelystad",
	270: "Leeuwarden",
	273: "Marknesse",
	275: "Deelen",
	277: "Lauwersoog",
	278: "Heino",
	279: "Hoogeveen",
	280: "Eelde",
	283: "Hupsel",
	285: "Huibertgat",
	286: "NieuwBeerta",
	290: "Twenthe",
	308: "Cadzand",
	310: "Vlissingen",
	311: "Hoofdplaat",
	312: "Oosterschelde",
	313: "VlaktevanDeRaan",
	315: "Hansweert",
	316: "Schaar",
	319: "Westdorpe",
	323: "Wilhelminadorp",
	324: "Stavenisse",
	330: "HoekvanHolland",
	331: "Tholen",
	340: "Woensdrecht",
	343: "RotterdamGeulhaven",
	344: "Rotterdam",
	348: "CabauwMast",
	350: "Gilze-Rijen",
	356: "Herwijnen",
	370: "Eindhoven",
	375: "Volkel",
	377: "Ell",
	380: "Maastricht",
	391: "Arcen",
}
#
def stn2str (stn):
	if stn in stn2stnm:
		return '"%s"' % stn2stnm [stn]
	else:
		return str (stn)



#
# Collect samples as a mapping from MM-DD to Q
#
row_iter = csv.DictReader (open ('knmi-10y.csv'))
samples = { }
stns = [ ]
totfnd = 0
totlen = 0
for row in row_iter:
	totlen += 1
	try:
		stn = int (row ['STN'     ])
		q   = int (row ['Q'       ])
		yyyymmdd = row ['YYYYMMDD']
		yyyy = int (yyyymmdd [0:4])
		mm   = int (yyyymmdd [4:6])
		dd   = int (yyyymmdd [6:8])
		d = datetime.date (yyyy, mm, dd)
	except:
		print ('Skipping faulty %r' % (row,), file=sys.stderr)
		continue
	if stn not in samples:
		stns.append (stn)
		samples [stn] = { }
	fnd = 0
	for o in range (-4,5):
		do = ( d + datetime.timedelta (o) ).isoformat ()
		#DEBUG# print ('Test against %s' % do, file=sys.stderr)
		if do in samplekeys:
			fnd += 1
			col = do [5:]
			if col in samples [stn]:
				samples [stn] [col].append (q)
			else:
				samples [stn] [col] = [q]
	assert fnd <= 1, 'Overlapping ranges: Found %04d-%02d-%02d again' % (yyyy,mm,dd)
	totfnd += fnd
#DEBUG# print (samples, file=sys.stderr)
#DEBUG# print ('Found %d out of %d' % (totfnd,totlen), file=sys.stderr)


#
# Derive the normal distributions per MM-DD
#
minQ = { }
maxQ = { }
cldQ = { }
hazQ = { }
stcol = [ ]
for stn in stns:
	for mm_dd in samples [stn]:
		#DEBUG# print ('#samples = %d' % len (samples), file=sys.stderr)
		nd = NormalDist.from_samples (samples [stn] [mm_dd])
		#DEBUG# print ('%s => %r' % (stn_mm_dd,nd), file=sys.stderr)
		#OLD# (stn,mm_dd) = stn_mm_dd.split ('-', 1)
		#
		# Find the data point for 2.5% and 97.5% via inverse CDF
		#
		m = nd.mean
		s = nd.stdev
		(low,hazy,high)  = map (nd.inv_cdf, [0.025, 0.80, 0.975])
		#
		cloud_cost = low / high
		hazy_cost = hazy / high
		#
		if stn not in minQ:
			minQ [stn] = { }
			maxQ [stn] = { }
			hazQ [stn] = { }
			cldQ [stn] = { }
		if mm_dd not in stcol:
			stcol.append (mm_dd)
		minQ [stn] [mm_dd] = low
		maxQ [stn] [mm_dd] = high
		hazQ [stn] [mm_dd] = hazy_cost
		cldQ [stn] [mm_dd] = cloud_cost

#
# Produce CSV output, with rows started by STN and Qmin/Qmax/Qcld
#
print ('"STN","VARkind","' + '","'.join (stcol) + '"')
for stn in stns:
	print (stn2str(stn) + ',"Qmin",' + ','.join (
		[ '%.3f' % minQ [stn].get (colk,'NaN') for colk in stcol]))
	print (stn2str(stn) + ',"Qmax",' + ','.join (
		[ '%.3f' % maxQ [stn].get (colk,'NaN') for colk in stcol]))
	print (stn2str(stn) + ',"Qhaz",' + ','.join (
		[ '%.3f' % hazQ [stn].get (colk,'NaN') for colk in stcol]))
	print (stn2str(stn) + ',"Qtio",' + ','.join (
		[ '%.3f' % cldQ [stn].get (colk,'NaN') for colk in stcol]))
