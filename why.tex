\section{Why to Reflect Light}

The idea to avoid overheating through reflection makes intuitive sense.
Hans Mulder added a physical reason, namely that reflected light can make it back into outer space without being transformed into heat.
A wonderful idea to slow down climate change (but not a excuse for politicians or anyone else to sit back and relax).

Climate warming is caused by heat that is trapped under \coo, along with added moisture in the air.
This is called the (disturbed) energy balance of the Earth.
The disbalance amounts to only $0.9\,W/m^2$, according to
\href{https://www.knmi.nl/kennis-en-datacentrum/achtergrond/energiebalans-van-de-aarde}{KNMI},
or $0.8\,W/m^2$ according to
\href{https://earthobservatory.nasa.gov/features/EnergyBalance}{NASA}.
Note that this figure will rise as we collect more \coo in our atmosphere, and remember that this danger poses self-accelerating aspects.  So we will work with a rounded figure of $1\,W/m^2$ below.

\paragraph{Example.}
Imagine a South-facing window in the moderate Dutch climate.
The window reflects the Sun over an area of $A_\textrm{w}=2\,m^2$ and is part of a $A_\textrm{h.g}=100\,m^2$ home-and-garden plot.
It uses aluminum lamell\ae, which would bounce of $\eta_\textrm{alu}=85\%$ of the light.
The lamell\ae\ are positioned under a $45\degrees$ angle, intended to bounce all the direct light in the day, in upward directions, after checking that it does not blind any neighbours in sight.
When the energy is not trapped into a home or onto a cloth, it will not be turned to heat and therefore do some correction to the energy balance of the Earth.
The same idea applies to heat pipes with reflectors for an overdose of Sun in Summer.

\paragraph{Incoming light.}
The light that hits the Earth depends on the season, and on the place on Earth.
For the Netherlands, Summer delivers a daily amount of light between 500 and 3000$\,J/cm^2$.
This information can be interactively queried as the parameter Q at
[KNMI daggegevens](https://www.knmi.nl/nederland-nu/klimatologie/daggegevens).
The $Q$ value is measured as light falling on the horizontal plane from any direction, so it does not just measure incoming light directly from the Sun.
Let's work with the lower end of this range.
\begin{eqnarray}
Q &=&
	500\,J/cm^2/d\\
	&=& 500\,J/cm^2/d \cdot (100\,cm/m)^2 \\
	&=& 5,000,000\,J/m^2/d \\
	&=& 5,000,000\,J/m^2/d \over 24\,h/d \cdot 3,600\,s/h \\
	&=& 57.87\,J/s/m^2 \\
	&=& 57.87\,W/m^2
\end{eqnarray}

\paragraph{Reflection.}
The area of $2\,m^2$ reflects light under angles up to $63\degrees$ (Dutch Summer Sun).
We shall ignore that this is not the condition throughout the day, in return for calculating with the lower bound of incoming light.
The amount of reflection $R_\textrm{w}$ is
\begin{eqnarray}
R_\textrm{w}
	&=& \eta_\textrm{alu}\cdot A_\textrm{w} \cdot \cos\,(63\degrees) \cdot Q \\
	&=& 0.85 \cdot 2\,m^2 \cdot 0.4540 \cdot 57.87\,W/m^2 \\
	&=& 42.04\,W
\end{eqnarray}

\paragraph{Energy Balance.}
What is the climate-heating energy $H_\textrm{w}$ that we would like to correct?
Note that we are doing this 2 out of 12 months, so we need to reflect six times as much,
\begin{eqnarray}
H_\textrm{w}
	&=& 6\cdot A_\textrm{h.g} \cdot 1\,W/m^2 \\
	&=& 6\cdot 100\,m^2 \cdot 1\,W/m^2 \\
	&=& 600\,W
\end{eqnarray}
The energy is not in balance, though this is an improvement.
We are correcting for 7\% with this window, and need about 14x more area that reflects.

\paragraph{More reflection on the roof.}
Horizontal areas work about twice as efficiently as vertical ones, because they do not incur the $\cos\,(63\degrees)=0.4540$ factor.
Areas on a flat roof could be used, but this might reduce the heat caught in Winter, unless slightly sloped to let in light for passive heating during those seasons.
This might be done with a ridged aluminum surface, mildly sloped under $30\degrees$ to $45\degrees$, such that the North side has the low end.
Good positions for this might be behind collectors for solar energy; in the Netherlands, roof installations must be as far from the rim of a flat roof as they are high; especially for heat collection device, this leaves a fair bit of space to bounce.
In the example home, there would be unexploited roof area on the North-side over $A_\textrm{r}=4.5\,m \cdot 2\,m=9\,m^2$.
The energy $R_\textrm{r}$ reflected here is
\begin{eqnarray}
R_\textrm{r}
	&=& \eta_\textrm{alu}\cdot A_\textrm{r} \cdot \cos\,(0\degrees) \cdot Q \\
	&=& 0.85 \cdot 9\,m^2 \cdot 1.0000 \cdot 57.87\,W/m^2 \\
	&=& 443\,W
\end{eqnarray}
If we used such an angle that this would reflect in the hottest 3 months of the year, we would need to reflect four times the energy disbalance,
\begin{eqnarray}
H_\textrm{r}
	&=& 4\cdot A_\textrm{h.g} \cdot 1\,W/m^2 \\
	&=& 4\cdot 100\,m^2 \cdot 1\,W/m^2 \\
	&=& 400\,W
\end{eqnarray}

\paragraph{Empowering.}
Including the roof top made a big difference, the balance made the turn to reduction of climate warming.
It is surprising and empowering that a single home could do that for itself.
South-facing windows and heat pipes are opportunities that all offer additional opportunities for reflection.
One word of gloom remains; this makes one's house and garden positive, but not any areal land, let alone the vast areas covered in oceans, and the area lost when polar caps melt.
So, this is a great way to help but not the full solution; as we hear so often, all technologies should be combined to confront the problems that our climate is facing.

